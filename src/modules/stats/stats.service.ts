import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { LCDClient } from '@terra-money/terra.js'
import { chain, mapKeys } from 'lodash'
import mysql from 'mysql'
import { constant } from 'src/constant'
import { RegisterTx } from 'src/entities/register-tx.entity'
import { RenewTx } from 'src/entities/renew-tx.entity'
import { SweepTx } from 'src/entities/sweep-tx.entity'
import { Repository } from 'typeorm'

export enum Granularity {
  YEARLY = 'yearly',
  QUARTERLY = 'quarterly',
  MONTHLY = 'monthly',
  WEEKLY = 'weekly',
  DAILY = 'daily',
  HOURLY = 'hourly',
}

const dateFormat = {
  [Granularity.YEARLY]: '%Y-01-01 00:00:00',
  [Granularity.QUARTERLY]: mysql.raw(
    'CONCAT("%Y-0", (MONTH(timestamp) - 1) DIV 3 * 3 + 1, "-01 00:00:00")'
  ),
  [Granularity.MONTHLY]: '%Y-%m-01 00:00:00',
  [Granularity.WEEKLY]: mysql.raw(
    'CONCAT("%Y-%m-", (DAY(timestamp) - 1) DIV 7 * 7 + 1, " 00:00:00")'
  ),
  [Granularity.DAILY]: '%Y-%m-%d 00:00:00',
  [Granularity.HOURLY]: '%Y-%m-%d %H:00:00',
}

@Injectable()
export class StatsService {
  private readonly lcdClient = new LCDClient({
    chainID: 'columbus-5',
    URL: 'https://lcd.terra.dev',
  })

  constructor(
    @InjectRepository(RegisterTx)
    private readonly registerTxRepository: Repository<RegisterTx>,
    @InjectRepository(RenewTx)
    private readonly renewTxRepository: Repository<RenewTx>,
    @InjectRepository(SweepTx)
    private readonly sweepTxRepository: Repository<SweepTx>
  ) {}

  async getOverallStats() {
    const [domainCount, registrations, renewals] = await Promise.all([
      this.getDomainCount(),
      this.getRegistrationsInfo(),
      this.getRenewalsInfo(),
    ])

    return { domainCount, registrations, renewals }
  }

  async getRegistrationsInfo() {
    const [result] = await this.registerTxRepository.query(/* sql */ `
      SELECT
        COUNT(1) AS _totalCount,
        SUM(paidAmount) AS _totalRevenue,
        SUM(CASE WHEN LENGTH(name) = 3 THEN 1 ELSE 0 END) AS _tier1Count,
        SUM(CASE WHEN LENGTH(name) = 3 THEN paidAmount ELSE 0 END) AS _tier1Revenue,
        SUM(CASE WHEN LENGTH(name) = 4 THEN 1 ELSE 0 END) AS _tier2Count,
        SUM(CASE WHEN LENGTH(name) = 4 THEN paidAmount ELSE 0 END) AS _tier2Revenue,
        SUM(CASE WHEN LENGTH(name) >= 5 THEN 1 ELSE 0 END) AS _tier3Count,
        SUM(CASE WHEN LENGTH(name) >= 5 THEN paidAmount ELSE 0 END) AS _tier3Revenue
      FROM register_tx
      WHERE method = "register"
    `)

    return mapKeys(result, (_, key) => key.substring(1))
  }

  async getRenewalsInfo() {
    const [result] = await this.renewTxRepository.query(/* sql */ `
      SELECT
        COUNT(1) AS _totalCount,
        SUM(paidAmount) AS _totalRevenue,
        SUM(CASE WHEN LENGTH(name) = 3 THEN 1 ELSE 0 END) AS _tier1Count,
        SUM(CASE WHEN LENGTH(name) = 3 THEN paidAmount ELSE 0 END) AS _tier1Revenue,
        SUM(CASE WHEN LENGTH(name) = 4 THEN 1 ELSE 0 END) AS _tier2Count,
        SUM(CASE WHEN LENGTH(name) = 4 THEN paidAmount ELSE 0 END) AS _tier2Revenue,
        SUM(CASE WHEN LENGTH(name) >= 5 THEN 1 ELSE 0 END) AS _tier3Count,
        SUM(CASE WHEN LENGTH(name) >= 5 THEN paidAmount ELSE 0 END) AS _tier3Revenue
      FROM renew_tx
    `)

    return mapKeys(result, (_, key) => key.substring(1))
  }

  async getRegistrations(granularity = Granularity.MONTHLY) {
    const format = dateFormat[granularity]

    const results = await this.registerTxRepository.query(
      /* sql */ `
        SELECT
          UNIX_TIMESTAMP(DATE_FORMAT(timestamp, ?)) AS _timestamp,
          COUNT(1) AS _totalCount,
          SUM(paidAmount) AS _totalRevenue,
          SUM(CASE WHEN LENGTH(name) = 3 THEN 1 ELSE 0 END) AS _tier1Count,
          SUM(CASE WHEN LENGTH(name) = 3 THEN paidAmount ELSE 0 END) AS _tier1Revenue,
          SUM(CASE WHEN LENGTH(name) = 4 THEN 1 ELSE 0 END) AS _tier2Count,
          SUM(CASE WHEN LENGTH(name) = 4 THEN paidAmount ELSE 0 END) AS _tier2Revenue,
          SUM(CASE WHEN LENGTH(name) >= 5 THEN 1 ELSE 0 END) AS _tier3Count,
          SUM(CASE WHEN LENGTH(name) >= 5 THEN paidAmount ELSE 0 END) AS _tier3Revenue
        FROM register_tx
        WHERE method = "register"
        GROUP BY _timestamp
        ORDER BY _timestamp ASC
      `,
      [format]
    )

    return chain(results)
      .map((result) => mapKeys(result, (_, key) => key.substring(1)))
      .value()
  }

  async getRenewals(granularity = Granularity.MONTHLY) {
    const format = dateFormat[granularity]

    const results = await this.renewTxRepository.query(
      /* sql */ `
        SELECT
          UNIX_TIMESTAMP(DATE_FORMAT(timestamp, ?)) AS _timestamp,
          COUNT(1) AS _totalCount,
          SUM(paidAmount) AS _totalRevenue,
          SUM(CASE WHEN LENGTH(name) = 3 THEN 1 ELSE 0 END) AS _tier1Count,
          SUM(CASE WHEN LENGTH(name) = 3 THEN paidAmount ELSE 0 END) AS _tier1Revenue,
          SUM(CASE WHEN LENGTH(name) = 4 THEN 1 ELSE 0 END) AS _tier2Count,
          SUM(CASE WHEN LENGTH(name) = 4 THEN paidAmount ELSE 0 END) AS _tier2Revenue,
          SUM(CASE WHEN LENGTH(name) >= 5 THEN 1 ELSE 0 END) AS _tier3Count,
          SUM(CASE WHEN LENGTH(name) >= 5 THEN paidAmount ELSE 0 END) AS _tier3Revenue
        FROM renew_tx
        GROUP BY _timestamp
        ORDER BY _timestamp ASC
      `,
      [format]
    )

    return chain(results)
      .map((result) => mapKeys(result, (_, key) => key.substring(1)))
      .value()
  }

  async getBuyBacks() {
    return await this.sweepTxRepository.find({ order: { timestamp: 'DESC' } })
  }

  async getDomainCount() {
    const { count } = await this.lcdClient.wasm.contractQuery<{ count: number }>(
      constant.address.registrar,
      { num_tokens: {} }
    )

    return count
  }
}
