import { Controller, Get, Query } from '@nestjs/common'
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger'
import { Cache } from 'src/decorators/cache.decorator'
import { Granularity, StatsService } from './stats.service'

@ApiTags('Statistics')
@Controller('stats')
export class StatsController {
  constructor(private readonly statsService: StatsService) {}

  @Cache(600) // 10 mins
  @ApiOperation({
    summary: 'Get overall statistics',
    description: 'Get overall statistics',
  })
  @Get('/')
  async getOverallStats() {
    return await this.statsService.getOverallStats()
  }

  @Cache(1800) // 30 mins
  @ApiOperation({
    summary: 'Get historical stats of registrations transactions',
    description: 'Get historical stats of registrations transactions',
  })
  @ApiQuery({ name: 'granularity', enum: Granularity })
  @Get('/registrations')
  async getRegistrations(@Query('granularity') granularity: Granularity) {
    return await this.statsService.getRegistrations(granularity)
  }

  @Cache(1800) // 30 mins
  @ApiOperation({
    summary: 'Get historical stats of renewal transactions',
    description: 'Get historical stats of renewal transactions',
  })
  @ApiQuery({ name: 'granularity', enum: Granularity })
  @Get('/renewals')
  async getRenews(@Query('granularity') granularity: Granularity) {
    return await this.statsService.getRenewals(granularity)
  }

  @Cache(1800) // 30 mins
  @ApiOperation({
    summary: 'Get buy back transactions',
    description: 'Get buy back transactions',
  })
  @Get('/buybacks')
  async getBuybacks() {
    return await this.statsService.getBuyBacks()
  }
}
