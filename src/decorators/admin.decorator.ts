import { applyDecorators, UseGuards } from '@nestjs/common'
import { ApiSecurity } from '@nestjs/swagger'
import { AdminGuard } from 'src/guards/admin.guard'

export const Admin = (): MethodDecorator & ClassDecorator => {
  return applyDecorators(ApiSecurity('api_key'), UseGuards(AdminGuard))
}
