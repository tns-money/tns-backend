import { GetTxListResult } from 'src/types/GetTxListResult'
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm'
import { AccountTxDump } from './account-tx-dump.entity'

@Entity()
export class TxDump {
  @PrimaryColumn()
  id: number

  @Column({ type: 'json' })
  txInfo: GetTxListResult['txs'][number]

  @OneToMany(() => AccountTxDump, (account) => account.txDump)
  accounts: AccountTxDump[]
}
