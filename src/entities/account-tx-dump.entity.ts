import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm'
import { TxDump } from './tx-dump.entity'

@Entity()
export class AccountTxDump {
  @PrimaryColumn()
  account: string

  @PrimaryColumn()
  txDumpId: number

  @ManyToOne(() => TxDump, (txDump) => txDump.accounts, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  txDump: TxDump
}
