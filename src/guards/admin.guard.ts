import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Request } from 'express'

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private readonly configService: ConfigService) {}

  canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest<Request>()

    const apiKey: string =
      req.query['api-key'] || req.headers['api-key'] || req.cookies['api-key']

    if (apiKey === this.configService.get<string>('apiKey')) {
      return true
    }

    throw new UnauthorizedException()
  }
}
