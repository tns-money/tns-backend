import {
  applyDecorators,
  CacheInterceptor,
  CacheTTL,
  UseInterceptors,
} from '@nestjs/common'

type Decorator = MethodDecorator & ClassDecorator

export const Cache = (ttl?: number): Decorator => {
  if (process.env.NODE_ENV === 'development') return applyDecorators()

  const decorators: Decorator[] = [UseInterceptors(CacheInterceptor)]

  if (ttl) decorators.push(CacheTTL(ttl))

  return applyDecorators(...decorators)
}
