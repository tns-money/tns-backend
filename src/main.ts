import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import cookieParser from 'cookie-parser'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })

  app.use(cookieParser())

  const options = new DocumentBuilder()
    .setTitle('Terra Name Service API')
    .setDescription('Terra Name Service REST API Documentations')
    .setVersion('1.0')
    .addApiKey({ type: 'apiKey', name: 'api-key', in: 'header' })

  const document = SwaggerModule.createDocument(app, options.build())

  SwaggerModule.setup('swagger', app, document, {
    swaggerOptions: { persistAuthorization: true },
  })

  const port = app.get(ConfigService).get('port')
  await app.listen(port)
}

bootstrap()
