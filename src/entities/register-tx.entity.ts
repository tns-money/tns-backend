import { Column, Entity, PrimaryColumn } from 'typeorm'

export enum RegisterMethod {
  REGISTER = 'register',
  OWNER_REGISTER = 'owner_register',
}

@Entity()
export class RegisterTx {
  @PrimaryColumn()
  txId: number

  @PrimaryColumn()
  msgIndex: number

  @Column()
  name: string

  @Column({ type: 'decimal', precision: 18, scale: 6 })
  paidAmount: number

  @Column({ type: 'enum', enum: RegisterMethod, default: RegisterMethod.REGISTER })
  method: RegisterMethod

  @Column()
  timestamp: Date

  @Column()
  tokenId: string

  @Column()
  sender: string

  @Column()
  txhash: string
}
