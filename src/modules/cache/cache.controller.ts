import { CACHE_MANAGER, Controller, Inject, Post } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { Cache } from 'cache-manager'
import { Admin } from 'src/decorators/admin.decorator'

@Controller('cache')
export class CacheController {
  constructor(@Inject(CACHE_MANAGER) private readonly cacheManager: Cache) {}

  @Admin()
  @Post('/reset')
  @ApiTags('Cache')
  async resetCache() {
    await this.cacheManager.reset()
    return { success: true }
  }
}
