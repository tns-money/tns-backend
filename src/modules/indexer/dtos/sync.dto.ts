import { IsEnum } from 'class-validator'
import { SyncType } from '../indexer.service'

export class SyncDto {
  @IsEnum(SyncType)
  readonly syncType: SyncType = SyncType.HEAD
}
