export const constant = {
  address: {
    registrar: 'terra1fsfnnv08cgwcyfyh3p89rf44tfy4tlgnsapyda',
    controller: 'terra1jfk9saed6c4n40qwe54mcxmgs8kz4husly3jx4',
    collector: 'terra18lhpvps8hay8mtt3vd0cptukzhmlvpssq79h5r',
  },
  initTxId: {
    registrar: 172381129,
    controller: 172381329,
    collector: 203918922,
  },
}
