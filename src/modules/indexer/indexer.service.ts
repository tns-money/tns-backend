import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client/core'
import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import axios from 'axios'
import { Cache } from 'cache-manager'
import fetch from 'cross-fetch'
import delay from 'delay'
import { chain } from 'lodash'
import prettyMs from 'pretty-ms'
import { constant } from 'src/constant'
import { AccountTxDump } from 'src/entities/account-tx-dump.entity'
import { NftOwner } from 'src/entities/nft-owner.entity'
import { RegisterMethod, RegisterTx } from 'src/entities/register-tx.entity'
import { RenewTx } from 'src/entities/renew-tx.entity'
import { SweepTx } from 'src/entities/sweep-tx.entity'
import { TxDump } from 'src/entities/tx-dump.entity'
import { GetTxListResult } from 'src/types/GetTxListResult'
import { DeepPartial, LessThan, Repository } from 'typeorm'
import { StatsService } from '../stats/stats.service'

// txs visualization = [newest, __, head, __, tail, __, oldest]
// where: `newest` is the most recent tx from Terra
//        `oldest` is the first deployed tx
export enum SyncType {
  /** Sync from `newest` tx to `head` tx */
  HEAD = 'head',
  /** Sync from `tail` tx to `oldest` tx */
  TAIL = 'tail',
  /** Sync from `newest` tx to `oldest` */
  FULL = 'full',
}

type Bundle = {
  txInfo: TxDump['txInfo']
  logInfo: TxDump['txInfo']['logs'][number]
  msg: TxDump['txInfo']['tx']['value']['msg'][number]
}

type SyncTxsOptions = {
  label: string
  account: string
  syncType?: SyncType
  initTxId?: number
}

type ProcessBundlesOptions = {
  label: string
  account: string
  syncType?: SyncType
  headTxId: number
  tailTxId: number
  onBundles: (bundles: Bundle[]) => Promise<void>
}

@Injectable()
export class IndexerService {
  private mantleClient = new ApolloClient({
    link: new HttpLink({ uri: 'https://mantle.terra.dev', fetch }),
    defaultOptions: { query: { fetchPolicy: 'network-only' } },
    cache: new InMemoryCache(),
  })

  constructor(
    @InjectRepository(RegisterTx)
    private readonly registerTxRepository: Repository<RegisterTx>,
    @InjectRepository(RenewTx)
    private readonly renewTxRepository: Repository<RenewTx>,
    @InjectRepository(SweepTx)
    private readonly sweepTxRepository: Repository<SweepTx>,
    @InjectRepository(NftOwner)
    private readonly nftOwnerRepository: Repository<NftOwner>,
    @InjectRepository(AccountTxDump)
    private readonly accountTxDumpRepository: Repository<AccountTxDump>,
    @InjectRepository(TxDump)
    private readonly txDumpRepository: Repository<TxDump>,
    @Inject(CACHE_MANAGER)
    private readonly cacheManager: Cache,
    private readonly statsService: StatsService
  ) {}

  async syncTxs(options: SyncTxsOptions) {
    const { label, account, syncType = SyncType.HEAD, initTxId = 0 } = options

    const startTime = Date.now()

    const [headTxId, tailTxId, txCount] = await Promise.all([
      this.getTxDumpHeadTxId(account),
      this.getTxDumpTailTxId(account),
      this.accountTxDumpRepository.count({ account }),
    ])

    const endingTxId = syncType === SyncType.HEAD ? headTxId : initTxId
    const startingTxId = syncType === SyncType.TAIL ? tailTxId : undefined

    let nextTxId = startingTxId
    let newestTxId = 0
    let loopIndex = 0

    try {
      while (true) {
        const { data } = await axios.get<GetTxListResult>(
          'https://fcd.terra.dev/v1/txs',
          { params: { account, limit: 100, offset: nextTxId } }
        )

        if (loopIndex === 0) {
          newestTxId = data.txs[0].id
        }

        const txs = data.txs.filter((txInfo) => txInfo.logs.length > 0)

        // Speed-optimized upsert txDumps
        await this.txDumpRepository
          .createQueryBuilder()
          .insert()
          .orUpdate(['txInfo'])
          .values(
            txs.map((txInfo) => {
              // Remove useless field so it doesn't take too much space
              delete txInfo.raw_log
              return this.txDumpRepository.create({ id: txInfo.id, txInfo })
            })
          )
          .execute()

        // Speed-optimized upsert accountTxDumps
        await this.accountTxDumpRepository
          .createQueryBuilder()
          .insert()
          .orIgnore()
          .values(
            txs.map((txInfo) =>
              this.accountTxDumpRepository.create({ account, txDumpId: txInfo.id })
            )
          )
          .execute()

        const lastTxIdInPage = data.txs[data.txs.length - 1].id

        // Log overall sync status
        const overallPercent =
          newestTxId === endingTxId
            ? 100
            : ((newestTxId - lastTxIdInPage) / (newestTxId - endingTxId)) * 100

        console.log(
          '[Sync] %s synced %s% (time elapsed: %s)',
          label,
          overallPercent.toFixed(2),
          prettyMs(Date.now() - startTime)
        )

        // Log next tx id pointer
        console.log(`[Sync] ${label} next Tx ID: ${data.next}`)

        // No more txs, done
        if (!data.next) {
          console.log(`[Sync] ${label} reached deployment tx (ID: ${lastTxIdInPage})`)
          break
        }

        // Reached ending tx id
        if (endingTxId) {
          const endingTxInfo = data.txs.find((tx) => tx.id === endingTxId)

          if (endingTxInfo) {
            console.log(`[Sync] ${label} reached ending tx (ID: ${endingTxInfo.id})`)
            break
          }
        }

        // Add delay to prevent fcd rate limits
        await delay(200)

        nextTxId = data.next
        loopIndex++
      }

      const newTxCount = await this.accountTxDumpRepository.count({ account })
      const txCountDiff = newTxCount - txCount

      console.log(`[Sync] ${label} updated ${txCountDiff.toLocaleString()} txs`)

      return {
        syncType,
        timeElapsed: prettyMs(Date.now() - startTime),
        account,
        txCount: txCountDiff,
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log(error.response.data)
      }

      throw error
    } finally {
      console.log(`[Sync] ${label} txs sync took ${prettyMs(Date.now() - startTime)}`)
    }
  }

  async indexCollector(syncType = SyncType.HEAD) {
    const overallStartTime = Date.now()

    // Make sure txs are synced to the latest one
    const syncTxsResult = await this.syncTxs({
      label: 'Collector',
      account: constant.address.collector,
      initTxId: constant.initTxId.collector,
      syncType: SyncType.HEAD,
    })

    const [headTxId, tailTxId, sweepCount] = await Promise.all([
      this.getCollectorHeadTxId(),
      this.getCollectorTailTxId(),
      this.sweepTxRepository.count(),
    ])

    const processBundlesResult = await this.processBundles({
      label: 'Collector',
      account: constant.address.collector,
      syncType,
      headTxId,
      tailTxId,
      onBundles: async (bundles) => {
        // Index Sweep txs
        const sweepTxs = chain(bundles)
          .map((bundle) => {
            const { txInfo, logInfo, msg } = bundle

            // Ignore non-contract txs
            if (msg.type !== 'wasm/MsgExecuteContract') return null

            // Ignore non-sweep txs
            if (!msg.value.execute_msg.sweep) return null

            const event = logInfo.events.find((event) => event.type === 'wasm')

            if (!event) return null

            const offerAmount = event.attributes.find(
              (attribute) => attribute.key === 'offer_amount'
            )?.value

            const returnAmount = event.attributes.find(
              (attribute) => attribute.key === 'return_amount'
            )?.value

            if (offerAmount && returnAmount) {
              return this.sweepTxRepository.create({
                txId: txInfo.id,
                msgIndex: logInfo.msg_index,
                offerAmount: Number(offerAmount) / 1e6,
                returnAmount: Number(returnAmount) / 1e6,
                price: Number(offerAmount) / Number(returnAmount),
                timestamp: txInfo.timestamp,
                txhash: txInfo.txhash,
              })
            }
          })
          .compact()
          .value()

        // Speed-optimized upsert sweep txs
        await this.sweepTxRepository
          .createQueryBuilder()
          .insert()
          .orUpdate(['offerAmount', 'returnAmount', 'timestamp', 'txhash'])
          .values(sweepTxs)
          .execute()

        // Log sweep txs sync status
        console.log('[Collector] Sweep Added %s txs', sweepTxs.length.toLocaleString())
      },
    })

    const [newSweepCount] = await Promise.all([this.sweepTxRepository.count()])

    const sweepCountDiff = newSweepCount - sweepCount

    console.log(`Updated ${sweepCountDiff.toLocaleString()} Sweep Transactions`)

    await this.cacheManager.reset()

    return {
      timeElapsed: prettyMs(Date.now() - overallStartTime),
      indexCollector: {
        syncType,
        sweep: sweepCountDiff,
        timeElapsed: processBundlesResult.timeElapsed,
      },
      syncTxs: syncTxsResult,
    }
  }

  async indexController(syncType = SyncType.HEAD) {
    const overallStartTime = Date.now()

    // Make sure txs are synced to the latest one
    const syncTxsResult = await this.syncTxs({
      label: 'Controller',
      account: constant.address.controller,
      initTxId: constant.initTxId.controller,
      syncType: SyncType.HEAD,
    })

    const [headTxId, tailTxId, totalDomainCount, registerCount, renewCount] =
      await Promise.all([
        this.getControllerHeadTxId(),
        this.getControllerTailTxId(),
        this.statsService.getDomainCount(),
        this.registerTxRepository.count(),
        this.renewTxRepository.count(),
      ])

    const processBundlesResult = await this.processBundles({
      label: 'Controller',
      account: constant.address.controller,
      syncType,
      headTxId,
      tailTxId,
      onBundles: async (bundles) => {
        // Index Register txs
        const registerTxs = chain(bundles)
          .map((bundle) => {
            const { txInfo, logInfo, msg } = bundle

            // Ignore non-contract txs
            if (msg.type !== 'wasm/MsgExecuteContract') return null

            // Ignore non-register txs
            if (
              !msg.value.execute_msg.register &&
              !msg.value.execute_msg.owner_register
            ) {
              return null
            }

            const eventsString = JSON.stringify(logInfo.events)
            const tokenId = eventsString.match(/"token_id","value":"([a-z0-9]+?)"/)?.[1]

            // No token_id found, perhaps an error parsing events, or an error has occured at this tx?
            if (!tokenId) return null

            const registerTx: DeepPartial<RegisterTx> = {
              txId: txInfo.id,
              msgIndex: logInfo.msg_index,
              timestamp: txInfo.timestamp,
              txhash: txInfo.txhash,
              tokenId,
              sender: msg.value.sender,
            }

            // register txs
            if (msg.value.execute_msg.register) {
              return this.registerTxRepository.create({
                ...registerTx,
                name: msg.value.execute_msg.register.name as string,
                paidAmount:
                  parseInt(msg.value.coins.find((coin) => coin.denom === 'uusd').amount) /
                  1e6,
                method: RegisterMethod.REGISTER,
              })
            }

            // owner_register txs
            if (msg.value.execute_msg.owner_register) {
              return this.registerTxRepository.create({
                ...registerTx,
                name: msg.value.execute_msg.owner_register.name as string,
                paidAmount: 0,
                method: RegisterMethod.OWNER_REGISTER,
              })
            }

            return null
          })
          .compact()
          .value()

        // Index Renew txs
        const renewTxs = chain(bundles)
          .map((bundle) => {
            const { txInfo, logInfo, msg } = bundle

            // Ignore non-contract txs
            if (msg.type !== 'wasm/MsgExecuteContract') return null

            // Ignore non-renew txs
            if (!msg.value.execute_msg.renew) return null

            return this.renewTxRepository.create({
              txId: txInfo.id,
              msgIndex: logInfo.msg_index,
              name: msg.value.execute_msg.renew.name as string,
              paidAmount:
                parseInt(msg.value.coins.find((coin) => coin.denom === 'uusd').amount) /
                1e6,
              timestamp: txInfo.timestamp,
              sender: msg.value.sender,
              txhash: txInfo.txhash,
            })
          })
          .compact()
          .value()

        // Speed-optimized upsert register txs
        await this.registerTxRepository
          .createQueryBuilder()
          .insert()
          .orUpdate([
            'name',
            'paidAmount',
            'method',
            'timestamp',
            'tokenId',
            'sender',
            'txhash',
          ])
          .values(registerTxs)
          .execute()

        // Speed-optimized upsert renew txs
        await this.renewTxRepository
          .createQueryBuilder()
          .insert()
          .orUpdate(['name', 'paidAmount', 'timestamp', 'sender', 'txhash'])
          .values(renewTxs)
          .execute()

        // Log register txs sync status
        const registerCount = await this.registerTxRepository.count()

        console.log(
          `[Controller] Register Added %s txs - %s out of %s domains (%s%)`,
          registerTxs.length.toLocaleString(),
          registerCount.toLocaleString(),
          totalDomainCount.toLocaleString(),
          ((registerCount / totalDomainCount) * 100).toFixed(2)
        )

        // Log renew txs sync status
        console.log('[Controller] Renew Added %s txs', renewTxs.length.toLocaleString())
      },
    })

    const [newRegisterCount, newRenewCount] = await Promise.all([
      this.registerTxRepository.count(),
      this.renewTxRepository.count(),
    ])

    const registerCountDiff = newRegisterCount - registerCount
    const renewCountDiff = newRenewCount - renewCount

    console.log(`Updated ${registerCountDiff.toLocaleString()} Register Transactions`)
    console.log(`Updated ${renewCountDiff.toLocaleString()} Renew Transactions`)

    await this.cacheManager.reset()

    return {
      timeElapsed: prettyMs(Date.now() - overallStartTime),
      indexController: {
        syncType,
        register: registerCountDiff,
        renew: renewCountDiff,
        timeElapsed: processBundlesResult.timeElapsed,
      },
      syncTxs: syncTxsResult,
    }
  }

  private async processBundles(options: ProcessBundlesOptions) {
    const {
      label,
      syncType = SyncType.HEAD,
      account,
      headTxId,
      tailTxId,
      onBundles,
    } = options

    const [headDumpTxId, tailDumpTxId] = await Promise.all([
      this.getTxDumpHeadTxId(account),
      this.getTxDumpTailTxId(account),
    ])

    const startTime = Date.now()

    const startingTxId = syncType === SyncType.TAIL ? tailTxId : null
    const endingTxId = syncType === SyncType.HEAD ? headTxId : tailDumpTxId

    const limit = 1200
    let bundleCount = 0
    let nextTxId = startingTxId

    try {
      while (true) {
        const query = this.txDumpRepository
          .createQueryBuilder()
          .leftJoin(AccountTxDump, 'AccountTxDump', 'id = AccountTxDump.txDumpId')
          .where('AccountTxDump.account = :account', { account })
          .limit(limit)
          .orderBy('id', 'DESC')

        if (nextTxId) {
          query.andWhere({ id: LessThan(nextTxId) })
        }

        const txDumps = await query.getMany()

        // Extract each msg in tx
        const bundles = chain(txDumps)
          .flatMap((txDump) => {
            return txDump.txInfo.logs.map((logInfo) => ({
              txInfo: txDump.txInfo,
              logInfo,
              msg: txDump.txInfo.tx.value.msg[logInfo.msg_index],
            }))
          })
          .compact()
          .value()

        // Process bundles
        await onBundles(bundles)

        // Log overall sync status
        const oldestTxIdInPage = txDumps[txDumps.length - 1].id

        // TODO: Improve percent evaluation (use txCount/totalTxCount instead)
        const overallPercent =
          headDumpTxId === endingTxId
            ? 100
            : ((headDumpTxId - oldestTxIdInPage) / (headDumpTxId - endingTxId)) * 100

        console.log(
          '[%s] Bundle processing %s% (time elapsed: %s)',
          label,
          overallPercent.toFixed(2),
          prettyMs(Date.now() - startTime)
        )

        // Reached last page
        if (txDumps.length < limit) {
          console.log(`[${label}] Bundle reached oldest tx (ID: ${oldestTxIdInPage})`)
          break
        }

        // Reached ending tx id
        if (endingTxId) {
          const endingTxInfo = txDumps.find((txDump) => txDump.id === endingTxId)

          if (endingTxInfo) {
            console.log(`[${label}] Bundle reached ending tx (ID: ${endingTxInfo.id})`)
            break
          }
        }

        nextTxId = oldestTxIdInPage
        bundleCount += bundles.length
      }

      return { timeElapsed: prettyMs(Date.now() - startTime), bundleCount }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.log(error.response.data)
      }

      throw error
    } finally {
      console.log(`[${label}] Bundle processing took ${prettyMs(Date.now() - startTime)}`)
    }
  }

  private async getCollectorHeadTxId() {
    const [headRegisterTx] = await Promise.all([
      this.sweepTxRepository.createQueryBuilder().select('MAX(txId)', 'txId').getRawOne(),
    ])

    return Math.min(headRegisterTx.txId)
  }

  private async getCollectorTailTxId() {
    const [tailRegisterTx] = await Promise.all([
      this.sweepTxRepository.createQueryBuilder().select('MIN(txId)', 'txId').getRawOne(),
    ])

    return Math.max(tailRegisterTx.txId)
  }

  private async getControllerHeadTxId() {
    const [headRegisterTx, headRenewTx] = await Promise.all([
      this.registerTxRepository
        .createQueryBuilder()
        .select('MAX(txId)', 'txId')
        .getRawOne(),
      this.renewTxRepository.createQueryBuilder().select('MAX(txId)', 'txId').getRawOne(),
    ])

    return Math.min(headRegisterTx.txId, headRenewTx.txId)
  }

  private async getControllerTailTxId() {
    const [tailRegisterTx, tailRenewTx] = await Promise.all([
      this.registerTxRepository
        .createQueryBuilder()
        .select('MIN(txId)', 'txId')
        .getRawOne(),
      this.renewTxRepository.createQueryBuilder().select('MIN(txId)', 'txId').getRawOne(),
    ])

    return Math.max(tailRegisterTx.txId, tailRenewTx.txId)
  }

  private async getTxDumpHeadTxId(account: string) {
    const result = await this.accountTxDumpRepository
      .createQueryBuilder()
      .select('MAX(txDumpId)', 'id')
      .where({ account })
      .getRawOne<{ id: number }>()

    return result.id ?? 0
  }

  private async getTxDumpTailTxId(account: string) {
    const result = await this.accountTxDumpRepository
      .createQueryBuilder()
      .select('MIN(txDumpId)', 'id')
      .where({ account })
      .getRawOne<{ id: number }>()

    return result.id ?? 0
  }
}
