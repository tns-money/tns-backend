import { Entity, Column, PrimaryColumn } from 'typeorm'

@Entity()
export class Metadata {
  @PrimaryColumn()
  key: string

  @Column({ type: 'json' })
  value: any
}
