import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AccountTxDump } from 'src/entities/account-tx-dump.entity'
import { Metadata } from 'src/entities/metadata.entity'
import { NftOwner } from 'src/entities/nft-owner.entity'
import { RegisterTx } from 'src/entities/register-tx.entity'
import { RenewTx } from 'src/entities/renew-tx.entity'
import { SweepTx } from 'src/entities/sweep-tx.entity'
import { TxDump } from 'src/entities/tx-dump.entity'
import { StatsModule } from '../stats/stats.module'
import { IndexerController } from './indexer.controller'
import { IndexerService } from './indexer.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RegisterTx,
      RenewTx,
      SweepTx,
      Metadata,
      NftOwner,
      AccountTxDump,
      TxDump,
    ]),
    StatsModule,
  ],
  providers: [IndexerService],
  controllers: [IndexerController],
  exports: [IndexerService],
})
export class IndexerModule {}
