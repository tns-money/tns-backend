import { CacheModule, Module, ValidationPipe } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { APP_PIPE } from '@nestjs/core'
import { TypeOrmModule } from '@nestjs/typeorm'
import redisStore from 'cache-manager-redis-store'
import { ClientOpts as RedisClientOpts } from 'redis'
import { AppController } from './app.controller'
import { configuration } from './configuration'
import { CacheModule as CustomCacheModule } from './modules/cache/cache.module'
import { IndexerModule } from './modules/indexer/indexer.module'
import { StatsModule } from './modules/stats/stats.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: process.env.NODE_ENV !== 'development',
      envFilePath: ['.env'],
      load: [configuration],
      isGlobal: true,
    }),
    CacheModule.registerAsync<RedisClientOpts>({
      isGlobal: true,
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('redis.host'),
        port: configService.get('redis.port'),
        ttl: configService.get('redis.ttl'),
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('db.host'),
        port: configService.get('db.port'),
        username: configService.get('db.username'),
        password: configService.get('db.password'),
        database: configService.get('db.name'),
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        timezone: 'utc',
        synchronize: false,
        bigNumberStrings: false,
        logging: process.env.NODE_ENV === 'development',
        extra: { charset: 'utf8mb4_general_ci' },
      }),
    }),
    StatsModule,
    IndexerModule,
    CustomCacheModule,
  ],
  controllers: [AppController],
  providers: [
    { provide: APP_PIPE, useFactory: () => new ValidationPipe({ transform: true }) },
  ],
})
export class AppModule {}
