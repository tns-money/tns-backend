import { Entity, Column, PrimaryColumn } from 'typeorm'

@Entity()
export class NftOwner {
  @PrimaryColumn()
  tokenId: string

  @Column()
  owner: string
}
