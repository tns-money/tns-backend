import { Body, Controller, Post } from '@nestjs/common'
import { ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger'
import { constant } from 'src/constant'
import { Admin } from 'src/decorators/admin.decorator'
import { SyncDto } from './dtos/sync.dto'
import { IndexerService } from './indexer.service'

@ApiTags('Indexer')
@Controller('indexer')
export class IndexerController {
  constructor(private readonly indexerService: IndexerService) {}

  /************************************************
   * Indexing Routes
   ***********************************************/

  @Admin()
  @ApiOperation({ summary: 'Index all txs', description: 'Index all txs' })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/index')
  async indexAll(@Body() body: SyncDto) {
    const indexControllerResult = await this.indexerService.indexController(body.syncType)
    const indexCollectorResult = await this.indexerService.indexCollector(body.syncType)
    return { indexControllerResult, indexCollectorResult }
  }

  @Admin()
  @ApiOperation({
    summary: 'Index controller related txs',
    description: 'Index controller related txs',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/index/controller')
  async indexController(@Body() body: SyncDto) {
    return await this.indexerService.indexController(body.syncType)
  }

  @Admin()
  @ApiOperation({
    summary: 'Index collector related txs',
    description: 'Index collector related txs',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/index/collector')
  async indexCollector(@Body() body: SyncDto) {
    return await this.indexerService.indexCollector(body.syncType)
  }

  /************************************************
   * Syncing Routes
   ***********************************************/

  @Admin()
  @ApiOperation({
    summary: 'Sync txs from Terra FCD',
    description: 'Sync txs from Terra FCD',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/sync')
  async syncAllTxs(@Body() body: SyncDto) {
    const syncControllerResult = await this.indexerService.syncTxs({
      label: 'Controller',
      account: constant.address.controller,
      syncType: body.syncType,
      initTxId: constant.initTxId.controller,
    })

    const syncRegistrarResult = await this.indexerService.syncTxs({
      label: 'Registrar',
      account: constant.address.registrar,
      syncType: body.syncType,
      initTxId: constant.initTxId.registrar,
    })

    const syncCollectorResult = await this.indexerService.syncTxs({
      label: 'Collector',
      account: constant.address.collector,
      syncType: body.syncType,
      initTxId: constant.initTxId.collector,
    })

    return { syncControllerResult, syncRegistrarResult, syncCollectorResult }
  }

  @Admin()
  @ApiOperation({
    summary: 'Sync controller related txs from Terra FCD',
    description: 'Sync controller related txs from Terra FCD',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/sync/controller')
  async syncControllerTxs(@Body() body: SyncDto) {
    return await this.indexerService.syncTxs({
      label: 'Controller',
      account: constant.address.controller,
      syncType: body.syncType,
      initTxId: constant.initTxId.controller,
    })
  }

  @Admin()
  @ApiOperation({
    summary: 'Sync registrar related txs from Terra FCD',
    description: 'Sync registrar related txs from Terra FCD',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/sync/registrar')
  async syncRegistrarTxs(@Body() body: SyncDto) {
    return await this.indexerService.syncTxs({
      label: 'Registrar',
      account: constant.address.registrar,
      syncType: body.syncType,
      initTxId: constant.initTxId.registrar,
    })
  }

  @Admin()
  @ApiOperation({
    summary: 'Sync collector related txs from Terra FCD',
    description: 'Sync collector related txs from Terra FCD',
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @Post('/sync/collector')
  async syncCollectorTxs(@Body() body: SyncDto) {
    return await this.indexerService.syncTxs({
      label: 'Collector',
      account: constant.address.collector,
      syncType: body.syncType,
      initTxId: constant.initTxId.collector,
    })
  }
}
