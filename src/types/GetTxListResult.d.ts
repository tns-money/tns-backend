export type GetTxListResult = {
  limit: number
  next: number
  txs: {
    id: number
    chainId: string
    tx: {
      type: string
      value: {
        fee: { gas: string; amount: { denom: string; amount: string } }
        memo: string
        msg: {
          type: string
          value: {
            coins: { denom: string; amount: string }[]
            sender: string
            contract: string
            execute_msg: any
          }
        }[]
        signatures: { signature: string; pub_key: { type: string; value: string } }[]
      }
    }
    events: { type: string; attributes: { key: string; value: string }[] }[]
    logs: {
      msg_index: number
      log: { tax: string }
      events: { type: string; attributes: { key: string; value: string }[] }[]
    }[]
    height: string
    txhash: string
    raw_log: string
    gas_used: string
    timestamp: string
    gas_wanted: string
  }[]
}
