import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity()
export class RenewTx {
  @PrimaryColumn()
  txId: number

  @PrimaryColumn()
  msgIndex: number

  @Column()
  name: string

  @Column({ type: 'decimal', precision: 18, scale: 6 })
  paidAmount: number

  @Column()
  timestamp: Date

  @Column()
  sender: string

  @Column()
  txhash: string
}
