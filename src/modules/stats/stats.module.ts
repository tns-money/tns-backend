import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { NftOwner } from 'src/entities/nft-owner.entity'
import { RegisterTx } from 'src/entities/register-tx.entity'
import { RenewTx } from 'src/entities/renew-tx.entity'
import { SweepTx } from 'src/entities/sweep-tx.entity'
import { StatsController } from './stats.controller'
import { StatsService } from './stats.service'

@Module({
  imports: [TypeOrmModule.forFeature([RegisterTx, RenewTx, SweepTx, NftOwner])],
  providers: [StatsService],
  controllers: [StatsController],
  exports: [StatsService],
})
export class StatsModule {}
