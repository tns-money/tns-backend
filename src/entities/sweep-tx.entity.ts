import { Column, Entity, PrimaryColumn } from 'typeorm'

@Entity()
export class SweepTx {
  @PrimaryColumn()
  txId: number

  @PrimaryColumn()
  msgIndex: number

  @Column({ type: 'decimal', precision: 18, scale: 6 })
  offerAmount: number

  @Column({ type: 'decimal', precision: 18, scale: 6 })
  returnAmount: number

  @Column({ type: 'decimal', precision: 18, scale: 6 })
  price: number

  @Column()
  timestamp: Date

  @Column()
  txhash: string
}
